#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

//Linked List

typedef struct node {
    char c;
    struct node *next;
    struct node *prev;
} node_t;

node_t *push(node_t *head, char c) {
    /* 1. allocate node */
    node_t *node = (node_t *) malloc(sizeof(node_t));

    /* 2. put in the data  */
    node->c = c;

    /* 3. Make next of new node as head and previous as NULL */
    node->next = (head);
    node->prev = NULL;

    /* 4. change prev of head node to new node */
    if ((head) != NULL)
        (head)->prev = node;

    /* 5. move the head to point to the new node */
    (head) = node;
    return head;
}

void deleteNode(node_t **head_ref, node_t *del) {
    /* base case */
    if (*head_ref == NULL || del == NULL)
        return;

    /* If node to be deleted is head node */
    if (*head_ref == del)
        *head_ref = del->next;

    /* Change next only if node to be deleted is NOT the last node */
    if (del->next != NULL)
        del->next->prev = del->prev;

    /* Change prev only if node to be deleted is NOT the first node */
    if (del->prev != NULL)
        del->prev->next = del->next;

    /* Finally, free the memory occupied by del*/
    free(del);
    return;
}

const char lookup_unescape[256] = {
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1,
        -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, 117, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, 117, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

bool isNullSymbol(char *string);

void getTestedChars(node_t *current, char *testedChars);

bool isStrAllocated = false;

bool
is_hex(char c) {
    if (((c >= '0') && (c <= '9')) ||
        ((c >= 'A') && (c <= 'F')) || ((c >= 'a') && (c <= 'f'))) {
        return true;
    }

    return false;
}

bool
is_decodable(char *str) {
    if (str[0] != '%')
        return false;
    if (is_hex(str[1]) == false)
        return false;
    if (is_hex(str[2]) == false)
        return false;
    return true;
}

unsigned char
decode(char *dst) {
    return (unsigned char) ((lookup_unescape[(unsigned int) *(dst + 1)] << 4) +
                            (lookup_unescape[(unsigned int) *(dst + 2)] & 0xff));
}

char *
getUnescapedString(char *inputStr, int len, int *ret_len) {
    //    less than 3
    if (len < 3) {
        *ret_len = len;
        return inputStr;
    }
    //    equal 3
    if (len == 3) {
        if (is_decodable(&inputStr[0])) {
            char *outputString = malloc(sizeof(char) * (2));
            isStrAllocated = true;
            if (isNullSymbol(&inputStr[0])) {
                //todo Get instructions how to deal with null that can't be handled with decode() help func. replace by 'N' for now
                outputString[0] = 'N';
            } else {
                outputString[0] = decode(&inputStr[0]);
            }
            outputString[1] = '\0';
            *ret_len = len - 2;
            return outputString;
        } else {
            *ret_len = len;
            return inputStr;
        }
    }
    //    more than 3
    node_t *list = NULL;

    for (int j = 0; j < len; ++j) {
        list = push(list, inputStr[j]);
    }
    // defining the 3 tested chars and the rest of "all behind" string
    node_t *current = list->next->next;
    char *testedChar = malloc(4 * sizeof(char));
    while (current != NULL) {
        getTestedChars(current, &testedChar[0]);
        if (is_decodable(&testedChar[0])) {
            if (isNullSymbol(&testedChar[0])) {
                //todo Get instructions how to deal with null that can't be handled with decode() help func. replace by 'N' for now
                current->c = 'N';
            } else {
                char decodedChar = decode(&testedChar[0]);
                current->c = decodedChar;
            }
            //Removing the 2 hex that already decoded.
            deleteNode(&list, current->prev->prev);
            deleteNode(&list, current->prev);
            len = len - 2;
            if (len < 3) {
                break;
            }
            if ((current->c == '%') && (current->prev != NULL) && (current->prev->prev != NULL)) {
                getTestedChars(current, testedChar);
                if (is_decodable(testedChar)) {
                    continue;
                }
            }
        }
        current = current->next;
        //edge case when the decoded of the string were at the very end
        if ((NULL != current) && (NULL == (current->prev->prev))) {
            current = current->next;
        }
    }
    free(testedChar);
    char *outputString = malloc(sizeof(char) * (len + 1));
    isStrAllocated = true;
    outputString[len] = '\0';
    // Copy the list to the output array and free list.
    for (int j = (len - 1); j >= 0; j--) {
        outputString[j] = list->c;
        node_t *temp = list;
        list = list->next;
        free(temp);
    }
    *ret_len = len;
    return outputString;
}

void getTestedChars(node_t *current, char *testedChars) {
    testedChars[0] = current->c;
    testedChars[1] = current->prev->c;
    testedChars[2] = current->prev->prev->c;
    testedChars[3] = '\0';
}

bool isNullSymbol(char *string) {
    if ((string[0] == '%') && (string[1] == '0') && (string[2] == '0')) {
        return true;
    }
    return false;
}

int
main(int argc, char *argv[]) {
    int ret_len;
    if (argv[1] == NULL) {
        printf("%d,%s\n", 0, "");
        return 0;
    }
    char *ret_str = getUnescapedString(argv[1], strlen(argv[1]), &ret_len);
    printf("%d,%s\n", ret_len, ret_str);
    if (isStrAllocated) {
        free(ret_str);
    }
}