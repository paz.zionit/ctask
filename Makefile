CC=gcc
DECODE_BIN=decode

all: compile test

.PHONY: test

test:
	tests/run-tests.sh $(DECODE_BIN)

compile: decode.c
	$(CC) decode.c -o $(DECODE_BIN)

clean:
	rm -rf $(DECODE_BIN)
