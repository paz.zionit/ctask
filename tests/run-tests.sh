#!/bin/bash

set -e

TESTS_DIR="tests"
TESTS_FILE="tests.txt"
EXEC_LOC="./cmake-build-debug/untitled.exe"

if [[ ! -e ${TESTS_DIR} ]]; then
    echo "Error: please run this script from the project root."
    exit 1
fi

TESTS_PATH="${TESTS_DIR}/${TESTS_FILE}"
FAIL_SUITE=0
NUM_TEST=1

# Run tests
while L= read -r test; do
    test_arg=$(echo ${test} | cut -d ',' -f 1)
    expected_res=$(echo ${test} | cut -d ',' -f 2)
    expected_res="${#expected_res},${expected_res}"
    echo ${EXEC_LOC}
    actual_res=$(${EXEC_LOC} ${test_arg})
    result="success"
    if [[ ${actual_res} != ${expected_res} ]]; then
        result="failed"
        FAIL_SUITE=1
    fi

    res_len=$(echo ${actual_res} | cut -d "," -f 1)
    res_str=$(echo ${actual_res} | cut -d "," -f 2)
    echo "Test no.${NUM_TEST}:"
    echo "  Input: ${test_arg} Len: ${#test_arg}"
    echo "  Output: ${res_str} Len:${res_len}"
    echo "  Test result: ${result}"
    if [[ ${result} == 'failed' ]]; then
    echo "Diff:"
    fi
    echo "${actual_res}" > actualResult
    echo "${expected_res}" > expectedResult
    diff --color -u actualResult expectedResult
    echo ""

    NUM_TEST=$((NUM_TEST+1))
done < ${TESTS_PATH}

echo "Testing done."

if [[ ! -z ${FAIL_SUITE} ]]; then
   exit 1
fi
